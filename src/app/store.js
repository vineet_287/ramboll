import { configureStore } from "@reduxjs/toolkit";
// import counterReducer from "../features/counter/counterSlice";
import meteoriteReducer from "../features/meteorite/meteoriteSlice";

export default configureStore({
  reducer: {
    meteorite: meteoriteReducer
  }
});
