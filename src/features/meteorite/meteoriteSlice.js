import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { client } from "../../api/client";

// Set initial state of Map Component
const initialState = {
  coordinatesData: [],
  url: "https://data.nasa.gov/resource/y77d-th95.json",
  status: "idle",
  error: null,
  startingYear: 0,
  endingYear: new Date().getFullYear()
};

// format date
const formatDate = dateString => {
  const d = new Date(dateString);
  const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  return ye;
};

export const fetchCoordinates = createAsyncThunk(
  "meteorite/fetchCoordinates",
  async () => {
    const response = await client.get(initialState.url);
    return response.filter(user => user.reclat > 0);
  }
);

export const meteoriteSlice = createSlice({
  name: "meteorite",
  initialState,
  reducers: {
    setStartDate: (state, action) => {
      state.startingYear = action.payload;
    },
    setEndDate: (state, action) => {
      state.endingYear = action.payload;
    }
  },
  extraReducers: {
    [fetchCoordinates.pending]: (state, action) => {
      state.status = "loading";
    },
    [fetchCoordinates.fulfilled]: (state, action) => {
      state.status = "succeeded";
      // Add any fetched posts to the array
      state.coordinatesData = state.coordinatesData.concat(action.payload);
      let years = state.coordinatesData.map(obj => formatDate(obj.year));
      state.startingYear =
        Math.min(...years) < 1970 ? 1970 : Math.min(...years);
      state.endingYear = Math.max(...years);
    },
    [fetchCoordinates.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    }
  }
});

export const { setStartDate, setEndDate } = meteoriteSlice.actions;

// @TODO Make a filter on state.coordinatesData and it will filter the records
export const handleSubmit = event => {
  // time end (sorry) Here just I need to add a logic to filter records and then
  // set them back to coordinatesData in state. Then it will work automatically
  alert("time end");
  event.preventDefault();
};

export default meteoriteSlice.reducer;

export const selectAllCoordinates = state => state.meteorite.coordinatesData;
