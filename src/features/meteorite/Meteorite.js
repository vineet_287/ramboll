import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchCoordinates,
  setStartDate,
  setEndDate,
  handleSubmit
} from "./meteoriteSlice";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";

export function Meteorite() {
  // const data = useSelector(getCoordinates);
  const dispatch = useDispatch();

  // Get component states
  const apiStatus = useSelector(state => state.meteorite.status);
  const error = useSelector(state => state.meteorite.error);
  const coordinatesData = useSelector(state => state.meteorite.coordinatesData);
  const startingYear = useSelector(state => state.meteorite.startingYear);
  const endingYear = useSelector(state => state.meteorite.endingYear);

  useEffect(() => {
    if (apiStatus === "idle") {
      dispatch(fetchCoordinates());
    }
  }, [apiStatus, dispatch]);

  let content;

  if (apiStatus === "loading") {
    content = <div className="loader">Loading...</div>;
  } else if (apiStatus === "succeeded") {
    content = (
      <>
        <form onSubmit={handleSubmit}>
          <label>
            Start Year:
            <input
              type="text"
              value={startingYear}
              onChange={e => dispatch(setStartDate(e.target.value))}
            />
            End Year:
            <input
              type="text"
              value={endingYear}
              onChange={e => dispatch(setEndDate(e.target.value))}
            />
          </label>
          <input type="submit" value="Submit" />
        </form>
        <Map center={[45.4, -75.7]} zoom={2}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          />
          {coordinatesData.map(user => (
            <Marker key={user.id} position={[user.reclat, user.reclong]}>
              <Popup>
                Name: {user.name}
                <br />
                Year: {user.year}
              </Popup>
            </Marker>
          ))}
        </Map>
      </>
    );
  } else if (apiStatus === "failed") {
    content = <div>{error}</div>;
  }

  return <section>{content}</section>;
}
