import React from "react";
import { Meteorite } from "./features/meteorite/Meteorite";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Meteorite />
    </div>
  );
}

export default App;
